package com.shop;

import com.shop.bean.User;
import com.shop.bean.UserExample;
import com.shop.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test {
    //添加删除修改需要提交事务
    public static void main(String[] args) throws IOException {
//        --读取全局配置文件（InputStream）
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
//        --生成工厂对象(SqlSessionFactory)
        SqlSessionFactory sq = new SqlSessionFactoryBuilder().build(is);
//        --获取session会话对象【链接】
        SqlSession session = sq.openSession();
//        --通过session对象对数据库进行操作
//        session.insert("com.shop.mapper.UserMapper.insertUser");//第一个是sql的调用路径

        //获取mapper对象
        UserMapper mapper = session.getMapper(UserMapper.class);


//---------------------insert------------------------------
//        User user = new User();
//        user.setName("张三1");
//        user.setPassword("123");
//        //insert是添加所有字段的数据   insertSelective是有选择性的添加数据【非空的添加】
//        mapper.insertSelective(user);
//------------------insert---------------------------------

//---------------------update------------------------------
//        User user = new User();
////        user.setuId(9);
//        user.setName("张三丰");
//        user.setPassword("123");
//        //updateByPrimaryKey是根据主键的数据   updateByPrimaryKeySelective是有选择性的修改数据【非空的添加】
//        //mapper.updateByPrimaryKeySelective(user);

        //updateByExampleSelective是根据模板条件来修改的数据
        //修改名字为张三的数据   改成张三丰
//        UserExample example = new UserExample();//模板  and xx =xx and xx=xx;
//        //使用example的Criteria来添加条件
//        example.createCriteria().andNameEqualTo("张三");
//        mapper.updateByExampleSelective(user, example);
//------------------update---------------------------------

 //------------------delete---------------------------------
//        mapper.deleteByPrimaryKey(9);
//------------------delete---------------------------------

//------------------select---------------------------------
        //1、查询所有selectByExample
//        List<User> list = mapper.selectByExample(new UserExample());
//        for(User user:list){
//            System.out.println(user);
//        }
        //2、根据主键查询
//        User user = mapper.selectByPrimaryKey(2);
//        System.out.println(user);
        //3、条件查询
        UserExample example =  new UserExample();
        //通过内部类 Criteria 来添加操作条件
        example.createCriteria().andNameLike("%n%");
        //根据模板进行查询
        List<User> list = mapper.selectByExample(example);
        for(User user:list){
            System.out.println(user);
        }
 //------------------select---------------------------------


        //        --提交事务
        session.commit();
    }
}