package com.shop.service;

import com.shop.bean.User;

//user模块的规范方法接口
public interface UserService {

    //登录方法
    public User userLogin(User user);

}
