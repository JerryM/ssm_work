package com.shop.mapper;

import com.shop.bean.ProCar;
import com.shop.bean.ProCarExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProCarMapper {
    int countByExample(ProCarExample example);

    int deleteByExample(ProCarExample example);

    int insert(ProCar record);

    int insertSelective(ProCar record);

    List<ProCar> selectByExample(ProCarExample example);

    int updateByExampleSelective(@Param("record") ProCar record, @Param("example") ProCarExample example);

    int updateByExample(@Param("record") ProCar record, @Param("example") ProCarExample example);
}