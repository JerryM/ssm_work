package com.shop.mapper;

import com.shop.bean.Production;
import com.shop.bean.ProductionExample;
import com.shop.bean.ProductionWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductionMapper {
    int countByExample(ProductionExample example);

    int deleteByExample(ProductionExample example);

    int deleteByPrimaryKey(Integer proId);

    int insert(ProductionWithBLOBs record);

    int insertSelective(ProductionWithBLOBs record);

    List<ProductionWithBLOBs> selectByExampleWithBLOBs(ProductionExample example);

    List<Production> selectByExample(ProductionExample example);

    ProductionWithBLOBs selectByPrimaryKey(Integer proId);

    int updateByExampleSelective(@Param("record") ProductionWithBLOBs record, @Param("example") ProductionExample example);

    int updateByExampleWithBLOBs(@Param("record") ProductionWithBLOBs record, @Param("example") ProductionExample example);

    int updateByExample(@Param("record") Production record, @Param("example") ProductionExample example);

    int updateByPrimaryKeySelective(ProductionWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ProductionWithBLOBs record);

    int updateByPrimaryKey(Production record);
}