package com.shop.mapper;

import com.shop.bean.Proimg;
import com.shop.bean.ProimgExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProimgMapper {
    int countByExample(ProimgExample example);

    int deleteByExample(ProimgExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Proimg record);

    int insertSelective(Proimg record);

    List<Proimg> selectByExample(ProimgExample example);

    Proimg selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Proimg record, @Param("example") ProimgExample example);

    int updateByExample(@Param("record") Proimg record, @Param("example") ProimgExample example);

    int updateByPrimaryKeySelective(Proimg record);

    int updateByPrimaryKey(Proimg record);
}