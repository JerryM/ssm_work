package com.shop.bean;

public class Production {
    private Integer proId;

    private String name;

    private Double price;

    private String brank;

    private Integer pAcount;

    private Integer cId;

    private Integer bId;

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBrank() {
        return brank;
    }

    public void setBrank(String brank) {
        this.brank = brank == null ? null : brank.trim();
    }

    public Integer getpAcount() {
        return pAcount;
    }

    public void setpAcount(Integer pAcount) {
        this.pAcount = pAcount;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public Integer getbId() {
        return bId;
    }

    public void setbId(Integer bId) {
        this.bId = bId;
    }
}