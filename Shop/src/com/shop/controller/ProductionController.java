package com.shop.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shop.bean.Category;
import com.shop.bean.Production;
import com.shop.bean.ProductionWithBLOBs;
import com.shop.service.CategoryServce;
import com.shop.service.ProductionService;
import com.shop.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
public class ProductionController {

    //获取商品的业务层对象
    @Autowired
    ProductionService productionService;
    //获取商品分类的业务层对象
    @Autowired
    CategoryServce categoryServce;

    //添加商品的请求
    @RequestMapping("/insertProduction")
    public String insertProduction(ProductionWithBLOBs production, HttpSession session
            , @RequestParam("imgname") MultipartFile[] imgname) throws IOException {
        boolean isok = productionService.insert(production, imgname);
        if (isok) {
            //重新查询商品数据
            //
            return "businessIndex";
        } else {
            //保存错误信息
            session.setAttribute("production", production);
            session.setAttribute("message", "添加操作失败");
            // 跳转回到添加页面
            return "redirect:toInsertProductionPage";//toInsertProductionPage.jsp
        }
    }

    //根据商家id和关键字查询商品的请求
    @RequestMapping("/BusinessIndex")
    public String BusinessIndex(Integer bId, @RequestParam(required = false, defaultValue = "1") Integer pageNo,
                                @RequestParam(required = false, defaultValue = "") String search, HttpServletRequest request) {
        //开启分页       第一个参数为页码，第二个参数为显示的记录条数
        PageHelper.startPage(pageNo, Constant.Bus_PageSize);
        //查询
        List<ProductionWithBLOBs> list = productionService.queryByBusId(bId, search);
        //构建分页的对象[分页信息]
        PageInfo<ProductionWithBLOBs> pageInfo = new PageInfo<>(list);
        // 保存集合数据在属性作用域中
        request.setAttribute("ProductionList", list);
        // 保存分页对象在作用域中
        request.setAttribute("pageInfo", pageInfo);
        // 保存原来搜索的关键字，进行数据回显
        request.setAttribute("search", search);
        // 跳转到businessIndex.jsp商家首页显示
        return "businessIndex";
    }

    //消费者根据关键字查询商品的请求
    @RequestMapping("/queryProduction_Cid")
    public String queryProduction_Cid(@RequestParam(required = false, defaultValue = "1") Integer pageNo,
                                      @RequestParam(required = false, defaultValue = "") String search, HttpServletRequest request) {
        //开启分页       第一个参数为页码，第二个参数为显示的记录条数
        PageHelper.startPage(pageNo, Constant.Cus_PageSize);
        //查询
        List<ProductionWithBLOBs> list = productionService.queryProduction_Cid(search);
        //构建分页的对象[分页信息]
        PageInfo<ProductionWithBLOBs> pageInfo = new PageInfo<>(list);
        // 保存集合数据在属性作用域中
        request.setAttribute("ProductionList", list);
        // 保存分页对象在作用域中
        request.setAttribute("pageInfo", pageInfo);
        // 保存原来搜索的关键字，进行数据回显
        request.setAttribute("search", search);
        // 跳转到ProductionList.jsp页显示
        return "ProductionList";
    }

    //查询所有商品的请求
    @RequestMapping("/queryAllProduction")
    public String queryAllProduction(HttpServletRequest request) {
        //查询
        List<Production> list = productionService.queryAll();
        // 保存集合数据在属性作用域中
        request.setAttribute("ProductionList", list);
        // 跳转到productionList.jsp
        return "productionList";
    }

    //跳转添加商品页面的请求
    @RequestMapping("/toInsertProductionPage")
    public String toInsertProductionPage(HttpServletRequest request) {
        //CategoryList  查询所有的分类列表，并且保存在作用域中
        List<Category> CategoryList = categoryServce.queryAll();
        request.setAttribute("CategoryList", CategoryList);
        //
        return "ProductionInsert";//ProductionInsert.jsp
    }

    //跳转添加订单页面的请求   //快速购买。默认数量为1
    @RequestMapping("/toByProduction")
    public String toByProduction(Integer proId, HttpServletRequest request) {
        //查询商品信息，并且保存在作用域中
        Production production = productionService.queryById(proId);
        request.setAttribute("productDetail", production);
        request.setAttribute("num", 1);
        // 跳转页面
        return "buyproduction";//buyproduction.jsp
    }

    //查询所有商品的请求
//    @RequestMapping("/queryAllProduction")
//    @ResponseBody
//    public List<Production> queryAllProduction(){
//        return productionService.queryAll();
//    }

}
