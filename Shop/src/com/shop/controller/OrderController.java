package com.shop.controller;

import com.shop.bean.ProOrder;
import com.shop.bean.Production;
import com.shop.service.ProOrderService;
import com.shop.service.ProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class OrderController {

    //获取业务层的对象
    @Autowired
    ProOrderService proOrderService;

    //添加订单的请求   //下单
    @RequestMapping("/buyProduction")
    public String buyProduction(ProOrder proOrder, Integer num, Integer proId,HttpServletRequest request) {
        //查询商品信息，并且保存在作用域中
        boolean success = proOrderService.insert(proOrder, num, proId);
        //
        if(success){
            //保存一个提示信息
            request.setAttribute("message","下单成功，欢迎继续购买！");
            return "success";
        }else{
            // 跳转信息提醒页面
            //保存一个提示信息
            request.setAttribute("message","下单失败，请重新操作！");
            return "success";
        }
    }
}
