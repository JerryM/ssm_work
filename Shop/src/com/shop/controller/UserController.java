package com.shop.controller;

import com.shop.bean.User;
import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

//User模块的控制层代码  [管理员]
@Controller
public class UserController {

    //获取业务层的对象
    @Autowired
    UserService userService;

    //登录请求处理   属性作用域【session、request、application、page】  Model
    @RequestMapping("/login")
    @ResponseBody
    public HashMap login(User user) {
        //
        HashMap map = new HashMap();
        //调用业务层方法查询用户
        User loginUser = userService.userLogin(user);
        //简单的判断
        if (loginUser != null) {//登录成功
            //
            map.put("flag", true);
            //
            return map;
        } else {//登录失败的  \跳转回到登录页面
            //保存一个错误信息
            //
            map.put("flag", false);
            map.put("msg", "用户名或密码错误！");
            //
            return map;
        }
    }

    //登录请求处理   属性作用域【session、request、application、page】  Model
    @RequestMapping("/userLogin")
    public String login(User user, HttpServletRequest request) {
        //调用业务层方法查询用户
        User loginUser = userService.userLogin(user);
        //简单的判断
        if (loginUser != null) {//登录成功
            //跳转到首页
            return "index";//   解析  /index.jsp
        } else {//登录失败的  \跳转回到登录页面
            //保存一个错误信息
            request.setAttribute("msg", "用户名或密码错误！");
            request.setAttribute("user", user);
            //
            return "login";//   解析  /login.jsp
        }
    }

    //http://localhost:8080/springmvc/userLogin?name=admin&password=123
    //user的登录请求   userLogin的登录请求uri
//    @RequestMapping("/userLogin")
//    @ResponseBody   //把return的结果直接输出
//    public String login(String name, String password) {
//        //简单的判断
//        if (name.equals("admin") && password.equals("123456")) {//登录成功
//            return "sucessed!";
//        } else {//登录失败的
//            return "failed!";
//        }
//    }

    //1、能够以对象的形式来接收数据
    //2、以网页的形式来发起请求【页面的跳转】
//    @RequestMapping("/userLogin")
//    public String login(User user) {
//        //简单的判断
//        if (user.getName().equals("admin") && user.getPassword().equals("123456")) {//登录成功
//            //跳转到首页
//            return "index";//   解析  /index.jsp
//        } else {//登录失败的  \跳转回到登录页面
//            return "login";//   解析  /login.jsp
//        }
//    }


}
