package com.shop.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shop.bean.Customer;
import com.shop.bean.ProductionWithBLOBs;
import com.shop.service.CustomerService;
import com.shop.service.ProductionService;
import com.shop.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class CustomerController {

    @Autowired
    CustomerService CustomerService;

    //登录请求处理   属性作用域【session、request、application、page】  Model
    @RequestMapping("/CustomerLogin")
    public String login(Customer Customer, HttpServletRequest request) {
        //调用业务层方法查询用户
        Customer loginCustomer = CustomerService.CustomerLogin(Customer);
        //简单的判断
        if (loginCustomer != null) {//登录成功
            //将登录成功的用于对象保存在session作用域
            request.getSession().setAttribute("nowCustomer", loginCustomer);
            //跳转到首页
            return "index";//   解析  /index.jsp
        } else {//登录失败的  \跳转回到登录页面
            //保存一个错误信息
            request.setAttribute("msg", "用户名或密码错误！");
            request.setAttribute("Customer", Customer);
            //
            return "customerlogin";//   解析  /customerlogin.jsp
        }
    }

}
