package com.shop.controller;

import com.shop.bean.CategoryResult;
import com.shop.service.CategoryServce;
import com.shop.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class IndexController {

    @Autowired
    CategoryServce categoryServce;

    //queryCategoryResult
    @RequestMapping("/queryCategoryResult")
    @ResponseBody
    public HashMap queryCategoryResult(HttpServletRequest request){
        //
        HashMap map = new HashMap();
        //
        List<CategoryResult> List = categoryServce.queryCategoryResult();
        //x轴的数据是？分类名
        //y轴的数据是？数量
        // 获取x轴的数据  分类名    .collect(Collectors.toList())把返回的结果封装为一个list
        List<String> names = List.stream().map(entiy->{
            return entiy.getName();
        }).collect(Collectors.toList());
        // 获取y轴的数据  数量    .collect(Collectors.toList())把返回的结果封装为一个list
        List<Integer> nums = List.stream().map(entiy->{
            return entiy.getNums();
        }).collect(Collectors.toList());
        //        for(CategoryResult result : List){
        //            System.out.println(result.getName()+result.getNums());
        //        }
        //保存数据
        map.put("names",names);
        map.put("nums",nums);
        return map;//businesseChart.jsp
    }


//    // 定义一个文件上传的方法，并返回图片名
//    @RequestMapping("/uploadFile")
//    @ResponseBody
//    public String upLoad(MultipartFile[] detailImg) throws IOException {
//        for(MultipartFile detail :detailImg){
//            if (!detail.getOriginalFilename().equals("")) {
//                // 上传文件路径
//                try {
//                    // 获取输出流
//                    OutputStream os = new FileOutputStream(Constant.path + "\\" + detail.getOriginalFilename());
//                    // 获取输入流 CommonsMultipartFile 中可以直接得到文件的流
//                    BufferedInputStream is = new BufferedInputStream(detail.getInputStream());
//                    byte[] buff = new byte[1024];
//                    int temp;
//                    // 一个一个字节的读取并写入
//                    while ((temp = is.read(buff)) != (-1)) {
//                        os.write(buff, 0, temp);
//                    }
//                    os.flush();
//                    os.close();
//                    is.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        //返回文件名
//        return "true";
//    }

    // 定义一个文件上传的方法，并返回图片名
    @RequestMapping("/uploadFile")
    @ResponseBody
    public String upLoad(MultipartFile detailImg) throws IOException {
        if (!detailImg.getOriginalFilename().equals("")) {
            // 上传文件路径
            try {
                // 获取输出流
                OutputStream os = new FileOutputStream(Constant.path + "\\" + detailImg.getOriginalFilename());
                // 获取输入流 CommonsMultipartFile 中可以直接得到文件的流
                BufferedInputStream is = new BufferedInputStream(detailImg.getInputStream());
                byte[] buff = new byte[1024];
                int temp;
                // 一个一个字节的读取并写入
                while ((temp = is.read(buff)) != (-1)) {
                    os.write(buff, 0, temp);
                }
                os.flush();
                os.close();
                is.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        //返回文件名
        return detailImg.getOriginalFilename();
    }

}
