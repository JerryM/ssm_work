package com.shop.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shop.bean.Business;
import com.shop.bean.Production;
import com.shop.bean.ProductionWithBLOBs;
import com.shop.service.BusinessService;
import com.shop.service.ProductionService;
import com.shop.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class BusinessController {
    
    @Autowired
    BusinessService businessService;
    @Autowired
    ProductionService productionService;

    //登录请求处理   属性作用域【session、request、application、page】  Model
    @RequestMapping("/businessLogin")
    public String login(Business business, HttpServletRequest request) {
        //调用业务层方法查询用户
        Business loginBusiness = businessService.businessLogin(business);
        //简单的判断
        if (loginBusiness != null) {//登录成功
            //将登录成功的用于对象保存在session作用域
            request.getSession().setAttribute("nowBusiness",loginBusiness);
            //查询商家的商品【根据商家id查询商品】
            //开启分页       第一个参数为页码，第二个参数为显示的记录条数
            PageHelper.startPage(1, Constant.Bus_PageSize);
            List<ProductionWithBLOBs> ProductionList = productionService.queryByBusId(loginBusiness.getbId(),null);
            request.getSession().setAttribute("ProductionList",ProductionList);
            //构建分页的对象[分页信息]
            PageInfo<ProductionWithBLOBs> pageInfo = new PageInfo<>(ProductionList);
            // 保存分页对象在作用域中
            request.setAttribute("pageInfo", pageInfo);
            //查询热销商品
            //查询商品分类
            //跳转到商家首页
            return "businessIndex";//   解析  /businessIndex.jsp
        } else {//登录失败的  \跳转回到登录页面
            //保存一个错误信息
            request.setAttribute("msg", "用户名或密码错误！");
            request.setAttribute("business", business);
            //
            return "businesslogin";//   解析  /businesslogin.jsp
        }
    }

}
