package com.shop.bean;

//用于封装  分类商品信息的实体
public class CategoryResult extends Category{

    private Integer nums;

    public Integer getNums() {
        return nums;
    }

    public void setNums(Integer nums) {
        this.nums = nums;
    }
}
