package com.shop.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

//定义一个常量类
public class Constant {

    public static String path = "E:\\实训\\广州工商\\work\\img";

    // 商家每页显示的记录条数
    public static Integer Bus_PageSize = 4;
    // 消费者每页显示记录条数
    public static Integer Cus_PageSize = 6;



    //定义一个静态方法用于进行文件上传
    public static String upLoad(MultipartFile detailImg) throws IOException {
        if (!detailImg.getOriginalFilename().equals("")) {
            // 上传文件路径
            try {
                // 获取输出流
                OutputStream os = new FileOutputStream(Constant.path + "\\" + detailImg.getOriginalFilename());
                // 获取输入流 CommonsMultipartFile 中可以直接得到文件的流
                BufferedInputStream is = new BufferedInputStream(detailImg.getInputStream());
                byte[] buff = new byte[1024];
                int temp;
                // 一个一个字节的读取并写入
                while ((temp = is.read(buff)) != (-1)) {
                    os.write(buff, 0, temp);
                }
                os.flush();
                os.close();
                is.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        //返回文件名
        return detailImg.getOriginalFilename();
    }
}
