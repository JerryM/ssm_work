package com.shop.mapper;

import com.shop.bean.ProOrderItem;
import com.shop.bean.ProOrderItemExample;
import com.shop.bean.ProOrderItemKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProOrderItemMapper {
    int countByExample(ProOrderItemExample example);

    int deleteByExample(ProOrderItemExample example);

    int deleteByPrimaryKey(ProOrderItemKey key);

    int insert(ProOrderItem record);

    int insertSelective(ProOrderItem record);

    List<ProOrderItem> selectByExample(ProOrderItemExample example);

    ProOrderItem selectByPrimaryKey(ProOrderItemKey key);

    int updateByExampleSelective(@Param("record") ProOrderItem record, @Param("example") ProOrderItemExample example);

    int updateByExample(@Param("record") ProOrderItem record, @Param("example") ProOrderItemExample example);

    int updateByPrimaryKeySelective(ProOrderItem record);

    int updateByPrimaryKey(ProOrderItem record);
}