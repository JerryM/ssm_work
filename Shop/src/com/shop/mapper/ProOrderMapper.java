package com.shop.mapper;

import com.shop.bean.ProOrder;
import com.shop.bean.ProOrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProOrderMapper {
    int countByExample(ProOrderExample example);

    int deleteByExample(ProOrderExample example);

    int deleteByPrimaryKey(String ordercode);

    int insert(ProOrder record);

    int insertSelective(ProOrder record);

    List<ProOrder> selectByExample(ProOrderExample example);

    ProOrder selectByPrimaryKey(String ordercode);

    int updateByExampleSelective(@Param("record") ProOrder record, @Param("example") ProOrderExample example);

    int updateByExample(@Param("record") ProOrder record, @Param("example") ProOrderExample example);

    int updateByPrimaryKeySelective(ProOrder record);

    int updateByPrimaryKey(ProOrder record);
}