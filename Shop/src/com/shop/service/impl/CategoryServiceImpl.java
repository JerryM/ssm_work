package com.shop.service.impl;

import com.shop.bean.Category;
import com.shop.bean.CategoryExample;
import com.shop.bean.CategoryResult;
import com.shop.mapper.CategoryMapper;
import com.shop.service.CategoryServce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryServce {

    //获取mapper
    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public List<CategoryResult> queryCategoryResult() {
        return categoryMapper.queryCategoryResult();
    }

    @Override
    public boolean insert(Category Category) {
        return false;
    }

    @Override
    public boolean update(Category Category) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<Category> queryAll() {
        return categoryMapper.selectByExample(new CategoryExample());
    }

    @Override
    public Category queryById(Integer id) {
        return null;
    }
}
