package com.shop.service.impl;

import com.shop.bean.ProOrder;
import com.shop.bean.ProOrderItem;
import com.shop.mapper.ProOrderItemMapper;
import com.shop.mapper.ProOrderMapper;
import com.shop.service.ProOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ProOrderServiceImpl implements ProOrderService {

    //获取mapper
    @Autowired
    ProOrderMapper proOrderMapper;
    @Autowired
    ProOrderItemMapper proOrderItemMapper;

    @Override
    @Transactional//事务管理
    public boolean insert(ProOrder proOrder,Integer num,Integer pid) {
        //完善订单信息
        // 订单号
        //生成uuid
        String Ordercode = UUID.randomUUID().toString();
        proOrder.setOrdercode(Ordercode);
        // 下单时间
        proOrder.setCreatedate(new Date());
        //--添加订单记录【未付款】
        int res1 = proOrderMapper.insertSelective(proOrder);
        //--添加订单-商品中间信息记录
        ProOrderItem item = new ProOrderItem();
        item.setCusId(proOrder.getCusId()); item.setOrdercode(proOrder.getOrdercode());
        item.setNum(num);item.setpId(pid);
        int res2 = proOrderItemMapper.insertSelective(item);
        //返回结果
        return (res1>0)&&(res2>0);
    }

    @Override
    public boolean update(ProOrder ProOrder) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<ProOrder> queryAll() {
        return null;
    }

    @Override
    public ProOrder queryById(Integer id) {
        return null;
    }
}
