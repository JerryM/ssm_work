package com.shop.service.impl;

import com.shop.bean.Customer;
import com.shop.bean.CustomerExample;
import com.shop.mapper.CustomerMapper;
import com.shop.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service //业务层
public class CustomerServiceImpl implements CustomerService {

    //获取dao的对象 mapper
    @Autowired
    CustomerMapper CustomerMapper;

    //根据消费者的手机号码和密码进行登录校验
    @Override
    public Customer CustomerLogin(Customer Customer) {
        //
        CustomerExample example = new CustomerExample();
        //
        CustomerExample.Criteria criteria = example.createCriteria();
        //添加条件  手机号码及密码
        criteria.andPhoneEqualTo(Customer.getPhone()).andPasswordEqualTo(Customer.getPassword());
        //根据用户名和密码进行查询
        List<Customer> list = CustomerMapper.selectByExample(example);
        //判断结果集中是否有数据
        if (!list.isEmpty() && list.size() > 0) {
            return list.get(0);//返回第一个Customer数据
        } else {
            return null;
        }
    }

}
