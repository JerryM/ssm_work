package com.shop.service.impl;

import com.shop.bean.Business;
import com.shop.bean.Business;
import com.shop.bean.BusinessExample;
import com.shop.mapper.BusinessMapper;
import com.shop.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service //业务层
public class BusinessServiceImpl implements BusinessService {

    //获取dao的对象 mapper
    @Autowired
    BusinessMapper businessMapper;

    @Override
    public Business businessLogin(Business business) {
        //
        BusinessExample example = new BusinessExample();
        //
        BusinessExample.Criteria criteria = example.createCriteria();
        //添加条件  用户名及密码
        criteria.andNameEqualTo(business.getName()).andPasswordEqualTo(business.getPassword());
        //根据用户名和密码进行查询
        List<Business> list = businessMapper.selectByExample(example);
        //判断结果集中是否有数据
        if (!list.isEmpty() && list.size() > 0) {
            return list.get(0);//返回第一个Business数据
        } else {
            return null;
        }
    }

}
