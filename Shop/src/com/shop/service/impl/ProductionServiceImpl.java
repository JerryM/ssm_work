package com.shop.service.impl;

import com.shop.bean.Production;
import com.shop.bean.ProductionExample;
import com.shop.bean.ProductionWithBLOBs;
import com.shop.bean.Proimg;
import com.shop.mapper.ProductionMapper;
import com.shop.mapper.ProimgMapper;
import com.shop.service.ProductionService;
import com.shop.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ProductionServiceImpl implements ProductionService {

    //获取mapper对象
    @Autowired
    ProductionMapper productionMapper;
    @Autowired
    ProimgMapper proimgMapper;

    @Override
    public List<Production> queryAll() {
        //查询所有
        return productionMapper.selectByExample(new ProductionExample());
    }

    @Override
    public List<ProductionWithBLOBs> queryProduction_Cid(String search) {
        //
        ProductionExample example = new ProductionExample();
        //
        ProductionExample.Criteria criteria = example.createCriteria();
        //添加条件  如果search不为空则添加name的模糊查询的条件
        if(search!=null&&!search.equals("")){
            criteria.andNameLike("%"+search+"%");
        }
        //根据查询进行查询
        return productionMapper.selectByExampleWithBLOBs(example);
    }

    @Override
    @Transactional  //在当前方法中添加一个事务管理    @Transactional 声明式事务管理
    public boolean insert(ProductionWithBLOBs production, MultipartFile[] multipartFiles) throws IOException {//, MultipartFile[] detailImg) {
        //1、调用mapper中的添加操作
        int result = productionMapper.insertSelective(production);
        //测试查看是否有生成商品id回到商品对象中
        System.out.println("商品id：" + production.getProId());
        //2、循环上传文件、添加记录到proimg的表
        for (MultipartFile file : multipartFiles) {
            //文件上传
            String fileName = Constant.upLoad(file);
            //添加记录
            if (fileName != null && !fileName.equals("")) {
                //图片对象
                Proimg img = new Proimg();
                img.setpId(production.getProId());
                img.setImgname(fileName);
                //调用service的方法执行添加操作
                proimgMapper.insertSelective(img);
                System.out.println("文件名：" + fileName);
            }
        }
        //判断影响行数
        return result > 0 ? true : false;
    }

    @Override
    public boolean update(Production production) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }


    @Override
    public Production queryById(Integer id) {
        return productionMapper.selectByPrimaryKey(id);
    }

    //根据商家id查询商品
    @Override
    public List<ProductionWithBLOBs> queryByBusId(Integer bus_id, String search) {
        //
        ProductionExample example = new ProductionExample();
        //
        ProductionExample.Criteria criteria = example.createCriteria();
        //添加条件
        criteria.andBIdEqualTo(bus_id);
        // 如果search不为空则添加name的模糊查询的条件
        if(search!=null&&!search.equals("")){
            criteria.andNameLike("%"+search+"%");
        }
        //根据查询进行查询
        return productionMapper.selectByExampleWithBLOBs(example);
    }
}
