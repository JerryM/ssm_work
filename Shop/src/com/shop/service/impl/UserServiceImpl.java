package com.shop.service.impl;

import com.shop.bean.User;
import com.shop.bean.UserExample;
import com.shop.mapper.UserMapper;
import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//这个是UserService的实现类
@Service//识别为业务层代码
public class UserServiceImpl implements UserService {

    //获取Usermapper的对象
    @Autowired   //自动注入   依赖注入
            UserMapper userMapper;

    //实现登录操作的逻辑代码
    @Override
    public User userLogin(User user) {
        //
        UserExample example = new UserExample();
        //
        UserExample.Criteria criteria = example.createCriteria();
        //添加条件  用户名及密码
        criteria.andNameEqualTo(user.getName()).andPasswordEqualTo(user.getPassword());
        //根据用户名和密码进行查询
        List<User> list = userMapper.selectByExample(example);
        //判断结果集中是否有数据
        if (!list.isEmpty() && list.size() > 0) {
            return list.get(0);//返回第一个user数据
        } else {
            return null;
        }
    }
}
