package com.shop.service.impl;

import com.shop.bean.Proimg;
import com.shop.mapper.ProimgMapper;
import com.shop.service.ProimgService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProimgServiceImpl implements ProimgService {

    //获取mapper对象
    ProimgMapper proimgMapper;

    @Override
    public boolean insert(Proimg Proimg) {
        return proimgMapper.insertSelective(Proimg) > 0 ? true : false;
    }

    @Override
    public boolean update(Proimg Proimg) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<Proimg> queryAll() {
        return null;
    }

    @Override
    public Proimg queryById(Integer id) {
        return null;
    }
}
