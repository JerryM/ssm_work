package com.shop.service;

import com.shop.bean.Business;
import com.shop.bean.User;

//Business模块的规范方法接口
public interface BusinessService {

    //登录方法
    public Business businessLogin(Business business);

}
