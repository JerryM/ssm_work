package com.shop.service;

import com.shop.bean.Proimg;

import java.util.List;

public interface ProimgService {

    //添加
    public boolean insert(Proimg Proimg);

    public boolean update(Proimg Proimg);

    public boolean delete(Integer id);

    public List<Proimg> queryAll();

    public Proimg queryById(Integer id);
}
