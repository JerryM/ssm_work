package com.shop.service;

import com.shop.bean.Category;
import com.shop.bean.CategoryResult;

import java.util.List;

public interface CategoryServce {

    //查询分类商品统计信息
    List<CategoryResult> queryCategoryResult();

    //添加
    public boolean insert(Category Category);

    public boolean update(Category Category);

    public boolean delete(Integer id);

    public List<Category> queryAll();

    public Category queryById(Integer id);

}
