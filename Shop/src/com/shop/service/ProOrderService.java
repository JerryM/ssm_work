package com.shop.service;

import com.shop.bean.ProOrder;
import com.shop.bean.ProOrder;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

//订单的业务层接口
public interface ProOrderService {

    //添加
    public boolean insert(ProOrder ProOrder,Integer num,Integer pid);

    public boolean update(ProOrder ProOrder);

    public boolean delete(Integer id);

    public List<ProOrder> queryAll();

    public ProOrder queryById(Integer id);

}
