package com.shop.service;

import com.shop.bean.Production;
import com.shop.bean.ProductionWithBLOBs;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

//商品业务层接口
public interface ProductionService {

    //添加
    public boolean insert(ProductionWithBLOBs production, MultipartFile[] multipartFiles) throws IOException;

    public boolean update(Production production);

    public boolean delete(Integer id);

    public List<Production> queryAll();

    //消费者根据name模糊查询商品的方法
    public List<ProductionWithBLOBs> queryProduction_Cid(String search);

    public Production queryById(Integer id);

    //根据商家id查询所有商品  、 添加模糊查询的条件【name】
    public List<ProductionWithBLOBs> queryByBusId(Integer bus_id,String search);


}
