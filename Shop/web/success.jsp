<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>404</title>
<meta name="format-detection" content="telephone=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link href="css/googleapis.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/icon-font-linea.css">
<link rel="stylesheet" type="text/css" href="css/multirange.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="css/themify-icons.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/effect.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/404.css">
<link rel="stylesheet" type="text/css" href="css/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	</header>
	<!-- End Header Box -->
	<!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="customerIndex.jsp">Home</a></li>
							<li class="animate-default title-hover-red"><a href="#">提示页面</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content 404 Page -->
		<div class="relative container-web">
			<div class="container">
				<div class="row relative">
					<div class=" relative content-404">
						<p>${message}</p>
						<div class="btn-back">
							<p>Please try one of the following pages</p>
							<a href="customerIndex.jsp" class="animate-default button-hover-red">home
								page</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Content 404 Page -->
		<!-- Support -->
		<div class=" support-box full-width bg-red support_box_v2">
			<div class="container-web">
				<div class=" container">
					<div class="row">
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_free_ship_white-min.png" alt="Icon Free Ship"
								class="absolute" />
							<p>free shipping</p>
							<p>on order over $500</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_support_white-min.png" alt="Icon Supports"
								class="absolute">
							<p>support</p>
							<p>life time support 24/7</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_patner_white-min.png" alt="Icon partner"
								class="absolute">
							<p>help partner</p>
							<p>help all aspects</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="img/icon_phone_table_white-min.png"
								alt="Icon Phone Tablet" class="absolute">
							<p>contact with us</p>
							<p>+07 (0) 7782 9137</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content Box -->
	<!-- Footer Box -->
	<footer class="relative full-width">
	<div class=" top-footer full-width">
		<div class="clearfix container-web relative">
			<div class=" container">
				<div class="row">
					<div class="clearfix col-md-9 col-sm-12 clear-padding col-xs-12">
						<div class="clearfix text-subscribe">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<p>sign up for newsletter</p>
							<p>Get updates on discount & counpons.</p>
						</div>
						<div class="clearfix form-subscribe">
							<input type="text" name="email-subscribe"
								placeholder="Enter your email . . .">
							<button class="animate-default button-hover-red">subscribe</button>
						</div>
					</div>
					<div
						class="clearfix col-md-3 col-sm-12 col-xs-12 clear-padding social-box text-right">
						<a href="#"><img src="img/social_tw-min.png"
							alt="Icon Social Twiter"></a> <a href="#"><img
							src="img/social_fa-min.png" alt="Icon Social Facebook"></a> <a
							href="#"><img src="img/social_int-min.png"
							alt="Icon Social Instagram"></a> <a href="#"><img
							src="img/social_yt-min.png" alt="Icon Social Youtube" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix container-web relative">
		<div class=" container clear-padding">
			<div class="row">
				<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
					<p>my account</p>
					<ul class="list-footer">
						<li><a href="#">My Account</a></li>
						<li><a href="#">Login</a></li>
						<li><a href="#">My Cart</a></li>
						<li><a href="#">My Wishlist</a></li>
						<li><a href="#">My Compare</a></li>
					</ul>
				</div>
				<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
					<p>information</p>
					<ul class="list-footer">
						<li><a href="#">Information</a></li>
						<li><a href="#">Orders History</a></li>
						<li><a href="#">My Wishlist</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Site Map</a></li>
					</ul>
				</div>
				<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
					<p>our services</p>
					<ul class="list-footer">
						<li><a href="#">Product Recall</a></li>
						<li><a href="#">Gift Vouchers</a></li>
						<li><a href="#">Returns And Exchanges</a></li>
						<li><a href="#">Shipping Options</a></li>
						<li><a href="#">Terms Of Use</a></li>
					</ul>
				</div>
				<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
					<p>contact us</p>
					<ul class="icon-footer">
						<li><i class="fa fa-home" aria-hidden="true"></i> 262
							Milacina Mrest, Behansed, Paris</li>
						<li><i class="fa fa-envelope" aria-hidden="true"></i>
							contact@yourcompany.com</li>
						<li><i class="fa fa-phone" aria-hidden="true"></i>
							070-7782-9137</li>
						<li><i class="fa fa-fax" aria-hidden="true"></i>
							070-7782-9138</li>
						<li><i class="fa fa-clock-o" aria-hidden="true"></i> 09:00 AM
							- 18:00 PM</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class=" bottom-footer full-width">
		<div class="clearfix container-web">
			<div class=" container">
				<div class="row">
					<div class="clearfix col-md-7 clear-padding copyright">
						<p class="clear-margin">Copyright © 2020 by EngoCreative. All
							Rights .</p>
					</div>
					<div
						class="clearfix footer-icon-bottom col-md-5 float-right clear-padding">
						<div class="icon_logo_footer float-right">
							<img src="img/image_payment_footer-min.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</footer>
	</div>
	<!-- End Footer Box -->
	<script src="js/jquery-3.3.1.min.js" defer=""></script>
	<script src="js/bootstrap.min.js" defer=""></script>
	<script src="js/multirange.js" defer=""></script>
	<script src="js/owl.carousel.min.js" defer=""></script>
	<script src="js/sync_owl_carousel.js" defer=""></script>
	<script src="js/scripts.js" defer=""></script>
</body>
</html>
