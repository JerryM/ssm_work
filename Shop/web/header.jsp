<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!-- push menu-->
<div class="pushmenu menu-home5">
	<div class="menu-push">
		<span class="close-left js-close"><i class="fa fa-times f-20"></i></span>
		<div class="clearfix"></div>
		<form role="search" method="get" id="searchform" class="searchform"
			action="queryProduction_Cid">
			<div>
				<label class="screen-reader-text" for="q"></label> <input
					type="text" placeholder="Search for products" value="" name="search"
					id="q" autocomplete="off"> <input type="hidden" name="type"
					value="product">
				<button type="submit" id="searchsubmit">
					<i class="ion-ios-search-strong"></i>
				</button>
			</div>
		</form>
	</div>
</div>
<!-- Header Box -->
<div class="wrappage">
	<header class="relative full-width box-shadow">
		<div class="clearfix container-web relative">
			<div class=" container">
				<div class="row">
					<div class="clearfix header-content full-width relative">
						<div class="clearfix icon-menu-bar">
							<i
								class="data-icon data-icon-arrows icon-arrows-hamburger-2 icon-pushmenu js-push-menu"></i>
						</div>
						<div class="clearfix logo">
							<a href="index"><img alt="Logo" src="img/logo.png" /></a>
						</div>
						<div class="clearfix search-box relative float-left">
							<c:choose>
								<c:when test="${nowBusiness!=null}">
								<form id="form_query" method="GET" action="BusinessIndex">
									<input type="hidden" name="bId" value="${nowBusiness.bId}"/>
								</c:when>
							<c:otherwise>
									<form id="form_query" method="GET" action="queryProduction_Cid">
							</c:otherwise>
							</c:choose>
								<div class="clearfix category-box relative">
									<c:choose>
										<c:when test="${nowBusiness!=null}">
											<select name="cId">
												<option value="0">本店铺商品</option>
												<c:forEach items="${TypesList}" var="indexType">
													<c:choose>
														<c:when test="${indexType.cId==CategoryId}">
															<option value="${indexType.cId}" selected="selected">${indexType.name}</option>
														</c:when>
														<c:otherwise>
															<option value="${indexType.cId}">${indexType.name}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</select>
										</c:when>
									<c:otherwise >
									<select name="cId">
										<option value="0">所有商品</option>
										<c:forEach items="${CategoryList}" var="indexCategory">
											<c:choose>
												<c:when test="${indexCategory.cId==cId}">
													<option value="${indexCategory.cId}" selected="selected">${indexCategory.name}</option>
												</c:when>
												<c:otherwise>
													<option value="${indexCategory.cId}">${indexCategory.name}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</select>
									</c:otherwise>

									</c:choose>
								</div>
								<input name="search" type="text" value="${search}"> <input id="page"
									name="pageNo" type="hidden" value="1">
								<button type="submit" class="animate-default button-hover-red">Search</button>
							</form>
						</div>
						<div class="clearfix icon-search-mobile absolute">
							<i onClick="showBoxSearchMobile()"
								class="data-icon data-icon-basic icon-basic-magnifier"></i>
						</div>
						<!-- 用户登录等操作 -->
						<c:if test="${nowCustomer==null&&nowBusiness==null}">
							<a style="margin-left: 50px;background: #eb1a21;font-size: 18px;"
							   class="btn btn-danger"
								href="customerlogin.jsp" role="button">登录</a>
						</c:if>
						<c:if test="${nowCustomer!=null}">
							<div class="btn-group" >
								<button type="button" style="margin-left: 50px;background: #eb1a21;font-size: 18px;" class="btn btn-danger dropdown-toggle"
									data-toggle="dropdown">${nowCustomer.name}
									<span style="margin-left: 10px" class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
										<li><a href="customerIndex.jsp">个人中心</a></li>
										<li><a href="queryProCar?cusId=${nowCustomer.cusId}">购物车</a></li>
										<li><a href="toCustomerOrderIndex?id=${nowCustomer.cusId}">个人订单</a></li>
										<li><a href="customerInsertAcount.jsp">充值账户余额</a></li>
									<li><a href="logout">注销</a></li>
								</ul>
							</div>
						</c:if>
						<c:if test="${nowBusiness!=null}">
							<div class="btn-group" >
								<button type="button" style="margin-left: 50px;background: #eb1a21;font-size: 18px;" class="btn btn-danger dropdown-toggle"
									data-toggle="dropdown">${nowBusiness.name}
									<span style="margin-left: 10px" class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
										<li><a href="BusinessIndex?bId=${nowBusiness.bId}">店家首页</a></li>
										<li><a href="businesseChart.jsp">分类数据统计</a></li>
										<li><a href="businessUpdate.jsp">店家中心</a></li>
										<li><a href="toBusinessOrderIndex?id=${nowBusiness.bId}">订单列表</a></li>
										<li><a href="toInsertProductionPage">添加商品</a></li>
									<li><a href="logout">注销</a></li>
								</ul>
							</div>
						</c:if>
					</div>
				</div>

			</div>
		</div>
	</header>
</div>