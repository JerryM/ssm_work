<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <title>确定订单</title>
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="css/googleapis.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/icon-font-linea.css">
    <link rel="stylesheet" type="text/css" href="css/multirange.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/effect.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/cartpage.css">
    <link rel="stylesheet" type="text/css" href="css/contact.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
</header>
<!-- End Header Box -->
<!-- Content Box -->
<div class="relative full-width">
    <!-- Breadcrumb -->
    <div class="container-web relative">
        <div class="container">
            <div class="row">
                <div class="breadcrumb-web">
                    <ul class="clear-margin">
                        <li class="animate-default title-hover-red"><a href="#">Home</a></li>
                        <li class="animate-default title-hover-red"><a href="#">购买结算</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumb -->
    <div class="relative container-web">
        <div class="container">
            <div class="row relative">
                <div class="full-width relative top-checkout-box overfollow-hidden">
                    <form action="buyProduction" method="post">
                        <div
                                class="col-md-12 col-sm-12 col-xs-12 right-content-shoping relative clear-padding-right">
                            <p class="title-shoping-cart">确定订单</p>
                            <div class="full-width relative overfollow-hidden">
                                <div
                                        class="relative clearfix full-width product-order-sidebar border no-border-t no-border-r no-border-l">
                                    <div class="image-product-order-sidebar center-vertical-image">
                                        <img src="/img/${productDetail.imgs[0].imgname}" alt=""/>
                                    </div>
                                    <div class="relative info-product-order-sidebar">
                                        <p
                                                class="title-product top-margin-15-default animate-default title-hover-black">
                                            <a href="#">${productDetail.name} x ${num}</a>
                                        </p>
                                        <p class="price-product">$${productDetail.price}</p>
                                    </div>
                                </div>
                            </div>
                            <p class="title-shoping-cart">下单信息：</p>
                            <br>


                            <div class="relative">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p
                                                    class="intro-top-click top-margin-default bottom-margin-default">
                                                收货人：</p>
                                            <div class="field-wrap">
                                                </label> <input type="text" name="receiver" required
                                                                autocomplete="off"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <p
                                                    class="intro-top-click top-margin-default bottom-margin-default">
                                                联系号码：</p>
                                            <div class="field-wrap">
                                                </label> <input type="text" name="mobile" required
                                                                autocomplete="off"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p
                                                    class="intro-top-click top-margin-default bottom-margin-default">
                                                收货地址：</p>
                                            <div class="field-wrap">
                                                </label> <input type="text" value="${nowCustomer.address}" name="address"  required
                                                                autocomplete=" off" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <p
                                                    class="intro-top-click top-margin-default bottom-margin-default">
                                                备注：</p>
                                            <div class="field-wrap">
                                                </label> <input type="text" name="usermessage"/>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="title-shoping-cart">Cart Total</p>
                                    <div class="full-width relative cart-total bg-gray  clearfix">
                                        <div class="relative justify-content top-margin-15-default">
                                            <p class="bold">Total</p>
                                            <p class="text-red price-shoping-cart">$${productDetail.price*num}</p>
                                        </div>
                                    </div>
                                    <input type="hidden" name="proId" value="${productDetail.proId}"/>
                                    <input type="hidden" name="cusId" value="${nowCustomer.cusId}"/>
                                    <input type="hidden" name="totalprice" value="${productDetail.price*num}"/>
                                    <input type="hidden" name="num" value="${num}"/>
                                    <div
                                            class="full-width relative payment-box bg-gray top-margin-15-default clearfix">
                                        <ul
                                                class="check-box-custom list-radius title-check-box-black clear-margin clear-margin">
                                            <li><label class="">Check Payment <input
                                                    type="radio" name="payment" checked=""> <span
                                                    class="checkmark"></span>
                                            </label> <br>
                                                <p class="info-payment">Please send a check to Store
                                                    Name, Store Street, Store Town, Store State / County, Store
                                                    Postcode.</p></li>
                                        </ul>
                                    </div>
                                    <button type="submit"
                                            class="btn-proceed-checkout full-width top-margin-15-default">确定下单
                                    </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Content Checkout -->


        <!-- Footer Box -->
        <footer class="relative full-width">
            <div class=" top-footer full-width">
                <div class="clearfix container-web relative">
                    <div class=" container">
                        <div class="row">
                            <div class="clearfix col-md-9 col-sm-12 clear-padding col-xs-12">
                                <div class="clearfix text-subscribe">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <p>sign up for newsletter</p>
                                    <p>Get updates on discount & counpons.</p>
                                </div>
                                <div class="clearfix form-subscribe">
                                    <input type="text" name="email-subscribe"
                                           placeholder="Enter your email . . .">
                                    <button class="animate-default button-hover-red">subscribe</button>
                                </div>
                            </div>
                            <div
                                    class="clearfix col-md-3 col-sm-12 col-xs-12 clear-padding social-box text-right">
                                <a href="#"><img src="img/social_tw-min.png"
                                                 alt="Icon Social Twiter"></a> <a href="#"><img
                                    src="img/social_fa-min.png" alt="Icon Social Facebook"></a>
                                <a href="#"><img src="img/social_int-min.png"
                                                 alt="Icon Social Instagram"></a> <a href="#"><img
                                    src="img/social_yt-min.png" alt="Icon Social Youtube"/></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" bottom-footer full-width">
                <div class="clearfix container-web">
                    <div class=" container">
                        <div class="row">
                            <div class="clearfix col-md-7 clear-padding copyright">
                                <p class="clear-margin">Copyright © 2020 by EngoCreative.
                                    All Rights .</p>
                            </div>
                            <div
                                    class="clearfix footer-icon-bottom col-md-5 float-right clear-padding">
                                <div class="icon_logo_footer float-right">
                                    <img src="img/image_payment_footer-min.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- End Footer Box -->
    <script src="js/jquery-3.3.1.min.js" defer=""></script>
    <script src="js/bootstrap.min.js" defer=""></script>
    <script src="js/multirange.js" defer=""></script>
    <script src="js/owl.carousel.min.js" defer=""></script>
    <script src="js/sync_owl_carousel.js" defer=""></script>
    <script src="js/scripts.js" defer=""></script>
</body>
</html>