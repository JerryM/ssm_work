<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>添加出售商品</title>
<meta name="format-detection" content="telephone=no">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link href="css/googleapis.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/icon-font-linea.css">
<link rel="stylesheet" type="text/css" href="css/multirange.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="css/themify-icons.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/effect.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/cartpage.css">
<link rel="stylesheet" type="text/css" href="css/contact.css">
<link rel="stylesheet" type="text/css" href="css/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<!-- End Header Box -->
	<!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="#">Home</a></li>
							<li class="animate-default title-hover-red"><a href="#">添加出售商品</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content Checkout -->
		<div class="relative container-web">
			<div class="container">
				<div class="row relative">
					<div
						class="full-width relative top-checkout-box overfollow-hidden top-margin-default">
						<div
							class="col-md-12 col-sm-12 col-xs-12 clear-padding-left left-top-checkout"></div>
						<form class="form-horizontal style-form"
							action="insertProduction" enctype="multipart/form-data"
							method="post">
							<div class="form-group">
								<label class="col-sm-1 col-sm-1 control-label">商品名称</label>
								<div class="col-sm-5">
									<input type="hidden" name="bId" value="${nowBusiness.bId}" class="form-control">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="text" name="name" required autocomplete="off" />
									</div>
								</div>
								<label class="col-sm-1 col-sm-1 control-label">价格</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="text" name="price" required autocomplete="off" value="${production.price}" />
									</div>
								</div>
							</div>
							<br>
							<div class="form-group">
								<label class="col-sm-1 col-sm-1 control-label">商品分类</label>
								<div class="col-sm-5">
									<select name="cId" class="form-control">
										<c:forEach items="${CategoryList}" var="indexCategory">
											<option value="${indexCategory.cId}">${indexCategory.name}</option>
										</c:forEach>
									</select>
								</div>
								<label class="col-sm-1 col-sm-1 control-label">描述</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="text" name="details" required autocomplete="off"  value="${production.details}"/>
									</div>
								</div>
							</div>
							<br>
							<div class="form-group">
								<label class="col-sm-1 col-sm-1 control-label">库存</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="text" name="pAcount" required autocomplete="off"  value="${production.pAcount}" />
									</div>
								</div>
								<label class="col-sm-1 col-sm-1 control-label">品牌</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="text" name="brank" required autocomplete="off"  value="${production.brank}" />
									</div>
								</div>
							</div>
							<br>
							<div class="form-group">
								<label class="col-sm-1 col-sm-1 control-label">图片</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="file" name="imgname" required autocomplete="off" />
									</div>
								</div>
								<label class="col-sm-1 col-sm-1 control-label">图片</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="file" name="imgname" required autocomplete="off" />
									</div>
								</div>
							</div>
							<br>
							<div class="form-group">
								<label class="col-sm-1 col-sm-1 control-label">图片</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="file" name="imgname" required autocomplete="off" />
									</div>
								</div>
								<label class="col-sm-1 col-sm-1 control-label">图片</label>
								<div class="col-sm-5">
									<div class="field-wrap">
										<label><span class="req">*</span>
										</label> <input type="file" name="imgname" required autocomplete="off" />
									</div>
								</div>
							</div>
							<br>
							<div class="form-group">
								<label class="col-sm-1 col-sm-1 control-label">商品详情</label>
								<div class="col-sm-8">
									<textarea type="text" rows="5" name="information" class="form-control">${production.information}</textarea>
								</div>
								<div class="col-sm-1">
								</div>
							</div>
							<br>
							<div style="margin-left: 400px;margin-bottom: 180px" class="relative justify-content form-login-checkout">
								<button type="submit" class="animate-default button-hover-red">确认信息</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- End Content Checkout -->

			<!-- Footer Box -->
			<footer class="relative full-width">
			<div class=" top-footer full-width">
				<div class="clearfix container-web relative">
					<div class=" container">
						<div class="row">
							<div class="clearfix col-md-9 col-sm-12 clear-padding col-xs-12">
								<div class="clearfix text-subscribe">
									<i class="fa fa-envelope-o" aria-hidden="true"></i>
									<p>sign up for newsletter</p>
									<p>Get updates on discount & counpons.</p>
								</div>
								<div class="clearfix form-subscribe">
									<input type="text" name="email-subscribe"
										placeholder="Enter your email . . .">
									<button class="animate-default button-hover-red">subscribe</button>
								</div>
							</div>
							<div
								class="clearfix col-md-3 col-sm-12 col-xs-12 clear-padding social-box text-right">
								<a href="#"><img src="img/social_tw-min.png"
									alt="Icon Social Twiter"></a> <a href="#"><img
									src="img/social_fa-min.png" alt="Icon Social Facebook"></a> <a
									href="#"><img src="img/social_int-min.png"
									alt="Icon Social Instagram"></a> <a href="#"><img
									src="img/social_yt-min.png" alt="Icon Social Youtube" /></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class=" bottom-footer full-width">
				<div class="clearfix container-web">
					<div class=" container">
						<div class="row">
							<div class="clearfix col-md-7 clear-padding copyright">
								<p class="clear-margin">Copyright © 2020 by EngoCreative.
									All Rights .</p>
							</div>
							<div
								class="clearfix footer-icon-bottom col-md-5 float-right clear-padding">
								<div class="icon_logo_footer float-right">
									<img src="img/image_payment_footer-min.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</footer>
		</div>
		<!-- End Footer Box -->
		<script src="js/jquery-3.3.1.min.js" defer=""></script>
		<script src="js/bootstrap.min.js" defer=""></script>
		<script src="js/multirange.js" defer=""></script>
		<script src="js/owl.carousel.min.js" defer=""></script>
		<script src="js/sync_owl_carousel.js" defer=""></script>
		<script src="js/scripts.js" defer=""></script>
</body>
</html>