<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <title>Home Page</title>
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="css/googleapis.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/icon-font-linea.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/effect.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/home.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">

</head>

<body>
<jsp:include page="header.jsp"></jsp:include>
<!-- End Header Box -->
<!-- End Header Box -->
<!-- Content Box -->
<div class="relative clearfix full-width">
    <!-- Menu & Slide -->
    <div class="clearfix container-web relative">
        <div class=" container relative">
            <div class="row">
                <div class="clearfix relative menu-slide clear-padding bottom-margin-default homepage-bottom-margin-default">
                    <!-- Menu -->
                    <div class="clearfix menu-web relative">
                        <ul>
                            <li><a href="queryProduction_Cid?cId=1"><img src="img/icon_hot.png" alt="Icon Hot Deals" /> <p>女装</p></a></li>
                            <li><a href="queryProduction_Cid?cId=2"><img src="img/icon_food.png" alt="Icon Food" /> <p>男装</p></a></li>
                            <li><a href="queryProduction_Cid?cId=3"><img src="img/icon_mobile.png" alt="Icon Mobile & Tablet" /> <p>鞋子</p></a></li>
                            <li><a href="queryProduction_Cid?cId=4"><img src="img/icon_electric.png" alt="Icon Electric Appliances" /> <p>美妆/个人护理</p></a></li>
                            <li><a href="queryProduction_Cid?cId=5"><img src="img/icon_computer.png" alt="Icon Electronics & Technology" /> <p>腕表/珠宝饰品</p></a></li>
                            <li><a href="queryProduction_Cid?cId=6"><img src="img/icon_fashion.png" alt="Icon Fashion" /> <p>手机/数码</p></a></li>
                            <li><a href="queryProduction_Cid?cId=7"><img src="img/icon_health.png" alt="Icon Health & Beauty" /> <p>玩具</p></a></li>
                            <li><a href="queryProduction_Cid?cId=8"><img src="img/icon_mother.png" alt="Icon Mother & Baby" /> <p>零食/茶酒</p></a></li>
                            <li><a href="queryProduction_Cid?cId=9"><img src="img/icon_book.png" alt="Icon Books & Stationery" /> <p>生鲜水果</p></a></li>
                            <li><a href="queryProduction_Cid?cId=10"><img src="img/icon_tablet.png" alt="Icon Home & Life" /> <p>大家电/生活电器</p></a></li>
                            <li><a href="queryProduction_Cid?cId=11"><img src="img/icon_sport.png" alt="Icon Sports & Outdoors" /> <p>医药保健</p></a></li>
                            <li><a href="queryProduction_Cid?cId=12"><img src="img/icon_auto.png" alt="Icon Auto & Moto" /> <p>家具建材</p></a></li>
                            <li><a href="queryProduction_Cid?cId=13"><img src="img/icon_voucher.png" alt="Icon Voucher Service" /> <p>图书影像</p></a></li>
                        </ul>
                    </div>
                    <!-- Slide -->
                    <div class="clearfix slide-box-home slide-v1 relative">
                        <div class="clearfix slide-home owl-carousel owl-theme">
                            <a href="toProductionDetails?proId=27" class="relative"><div class="item"><img src="img/banner.png" alt="Banner Header 1"></div></a>
                            <a href="toProductionDetails?proId=31" class="relative"><div class="item"><img src="img/slide_2.png" alt="Banner Header 2"></div></a>
                        </div>
                    </div>
                    <div class=" box-banner-small-v1 box-banner-small">
                        <div class="effect-layla relative clear-padding col-md-4 col-sm-4 col-xs-4 float-left zoom-image-hover">
                            <img src="img/banner_small.png" alt="">
                            <a href="toProductionDetails?proId=28" class="relative"></a>
                        </div>
                        <div class="effect-layla relative clear-padding col-md-4 col-sm-4 col-xs-4 float-left zoom-image-hover">
                            <img src="img/banner_small_1.png" alt="">
                            <a href="toProductionDetails?proId=29" class="relative"></a>
                        </div>
                        <div class="effect-layla relative clear-padding col-md-4 col-sm-4 col-xs-4 float-left zoom-image-hover">
                            <img src="img/banner_small_2.png" alt="">
                            <a href="toProductionDetails?proId=30" class="relative"></a>
                        </div>
                    </div>
                </div>
                <!-- End Menu & Slide -->
            </div>
        </div>
    </div>
    <!-- Content Product -->
    <div class="clearfix box-product full-width top-padding-default homepage-top-padding-default bg-gray">
        <div class="clearfix container-web">
            <div class=" container">
                <div class="row">
                    <!-- Title Product -->
                    <div class="clearfix title-box full-width bottom-margin-default homepage-bottom-margin-default border bg-white">
                        <div class="clearfix name-title-box title-hot-bg relative">
                            <img src="img/icon_percent.png" class="absolute" alt="Icon Hot Deals" />
                            <p>good deal today</p>
                        </div>
                        <div class="clearfix menu-title-box bold uppercase">
                            <ul>
                                <li><a onClick="showBoxCateHomeByID('#mobile-tablet','.good-deal-product')" href="javascript:;">mobile & tablet</a></li>
                                <li><a onClick="showBoxCateHomeByID('#food','.good-deal-product')" href="javascript:;">food</a></li>
                                <li><a onClick="showBoxCateHomeByID('#home-life','.good-deal-product')" href="javascript:;">home & life</a></li>
                                <li><a onClick="showBoxCateHomeByID('#fashion','.good-deal-product')" href="javascript:;">fashion</a></li>
                                <li><a onClick="showBoxCateHomeByID('#auto-moto','.good-deal-product')" href="javascript:;">auto & moto</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix content-product-box bottom-margin-default homepage-bottom-margin-default full-width">
                    <div class="row">
                        <div class="relative">
                            <div class="good-deal-product animate-default active-box-category hidden-content-box" id="mobile-tablet">
                                <!-- Product Son -->
                                <div class="owl-carousel owl-theme">
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/img_product.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="toProductionDetails?proId=17">安卓手机</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$2399.988</span> $$1999.99</p>
                                    </div>
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_1.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="toProductionDetails?proId=20">EB健型 乳清蛋白质营养粉蛋白</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$945.59</span> $788.0</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_2.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="toProductionDetails?proId=18">蓝牙耳机真无线</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$238.79</span> $199.0</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_3.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="toProductionDetails?proId=19">C.more皙摩海葡萄</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$460.79</span> $384.0</p>
                                    </div>
                                    <!-- End Product Son -->
                                </div>
                            </div>
                            <div class="good-deal-product animate-default hidden-content-box" id="food">
                                <!-- Product Son -->
                                <div class="owl-carousel owl-theme">
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_8-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Has eu idque similique</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_9-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Est cu nibh clita</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_15-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Beatvs Solo2 On-Ear</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_10-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Lorem ipsum dolor sit amet</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <!-- End Product Son -->
                                </div>
                            </div>
                            <div class="good-deal-product animate-default hidden-content-box" id="home-life">
                                <!-- Product Son -->
                                <div class="owl-carousel owl-theme">
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_13-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Pro at nostrud</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_14-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Pro at definitiones</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_6-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Nostrud percipit definitiones</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_7-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Sea accusata voluptatibus</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <!-- End Product Son -->
                                </div>
                            </div>
                            <div class="good-deal-product animate-default hidden-content-box" id="fashion">
                                <!-- Product Son -->
                                <div class="owl-carousel owl-theme">
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_11-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Ne cum falli voluptua</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_12-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Ne dolor voluptua</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_14-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Vel regione discere ut</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_6-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Sed an nominavi</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <!-- End Product Son -->
                                </div>
                            </div>
                            <div class="good-deal-product animate-default hidden-content-box" id="auto-moto">
                                <!-- Product Son -->
                                <div class="owl-carousel owl-theme">
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_4-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Te eam iisque deseruisse</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class=" product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_6-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Mauris varius</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_13-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Nullam vel lectus maximus</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <div class="clearfix product-son ">
                                        <div class="clearfix image-product relative animate-default">
                                            <div class="center-vertical-image">
                                                <img src="img/product_home_14-min.png" alt="Product . . ." />
                                            </div>
                                            <ul class="option-product animate-default">
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
                                                <li class="relative"><a href="#"><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix ranking">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half" aria-hidden="true"></i>
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                        </div>
                                        <p class="title-product clearfix full-width title-hover-black animate-default"><a class="animate-default" href="#">Maecenas justo ex</a></p>
                                        <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                    </div>
                                    <!-- End Product Son -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Content Product -->

    <!-- Banner Full With -->
    <div class="clearfix relative full-width bottom-margin-default homepage-bottom-margin-default">
        <div class="clearfix container-web">
            <div class=" container banner_full_width">
                <div class="row overfollow-hidden banners-effect5 relative">
                    <img src="img/banner_full_w.png" alt="Banner Full Width . . .">
                    <a href="#"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner Full With -->
    <!-- Product Box -->
    <div class=" container-web">
        <div class=" container">
            <div class="row">
                <div class="clearfix title-box full-width border">
                    <div class="clearfix name-title-box title-category title-jungle-green-bg relative">
                        <img alt="Icon Mobile & Tablet" src="img/icon_mobile.png" class="absolute" />
                        <p>mobile & tablet</p>
                    </div>
                    <div class="clearfix menu-title-box bold uppercase">
                        <ul>
                            <li><a onClick="showBoxCateHomeByID('#smart-phone','.box-mobile-content')" href="javascript:;">Smart phone</a></li>
                            <li><a onClick="showBoxCateHomeByID('#tablet','.box-mobile-content')" href="javascript:;">Tablet</a></li>
                            <li><a onClick="showBoxCateHomeByID('#smart-watch','.box-mobile-content')" href="javascript:;">Smart Watch</a></li>
                            <li><a onClick="showBoxCateHomeByID('#case','.box-mobile-content')" href="javascript:;">Case</a></li>
                            <li><a onClick="showBoxCateHomeByID('#gadget','.box-mobile-content')" href="javascript:;">Gadget</a></li>
                        </ul>
                    </div>
                </div>
                <div class="display-table bottom-margin-default homepage-bottom-margin-default full-width">
                    <div class="clearfix clear-padding list-logo-category list-logo-category-v1 float-left border no-border-t no-border-r">
                        <ul>
                            <li><a href="#"><img src="img/logo_3.png" alt="Logo"></a></li>
                            <li><a href="#"><img src="img/logo_4.png" alt="Logo"></a></li>
                            <li><a href="#"><img src="img/logo_5.png" alt="Logo"></a></li>
                            <li><a href="#"><img src="img/logo_6.png" alt="Logo"></a></li>
                            <li><a href="#"><img src="img/logo_1.png" alt="Logo"></a></li>
                            <li><a href="#"><img src="img/logo_2.png" alt="Logo"></a></li>
                        </ul>
                    </div>
                    <div class=" banner-category float-left relative effect-bubba zoom-image-hover">
                        <img src="img/banner_category_1-min.png" alt="Banner">
                        <a href="toProductionDetails?proId=25"></a>
                    </div>
                    <div class="clearfix list-products-category list-products-category-v1 float-left relative">
                        <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box active-box-category" id="smart-phone">
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_4-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="toProductionDetails?proId=21">蓝牙摆放音箱</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$600</span> $250</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_5-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=22"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="toProductionDetails?proId=22">苹果手机</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$500</span> $450</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_6-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=23"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="toProductionDetails?proId=23">智能盒子</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$500</span> $400</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_7-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=24"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="toProductionDetails?proId=24">摄像机</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$600</span> $300</p>
                                </div>
                            </div>
                        </div>
                        <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="tablet">
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_6-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Beatvs Solo2 On-Ear</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$540</span> $360</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_7-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=22"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Has eu idque similique</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$500</span> $400</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_4-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=23"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Vel regione discere ut</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$500</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_5-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=24"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Beatvs Solo2 On-Ear</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$607</span> $530</p>
                                </div>
                            </div>
                        </div>
                        <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="smart-watch">
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_5-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=22"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">MH02-Black09</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$350</span> $100</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_6-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=22"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Voyage Yoga Bag</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$500</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_4-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Wayfarer Messenger Bag</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_7-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Rival Field Messenger</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                </div>
                            </div>
                        </div>
                        <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="case">
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_4-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Impulse Duffle</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_6-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">MH01-Black</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_7-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Diam Special1</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_5-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Diam Special08</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                </div>
                            </div>
                        </div>
                        <div class="box-mobile-content border-collapsed-box animate-default hidden-content-box" id="gadget">
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_6-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=21"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Emile Henry Braiserr21</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_4-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=22"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">MH02-Black09</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$500</span> $350</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_5-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=23"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Rival Field Messenger</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$320</span> $206</p>
                                </div>
                            </div>
                            <div class="clearfix relative product-no-ranking border-collapsed-element percent-content-2">
                                <div class="effect-hover-zoom center-vertical-image">
                                    <img src="img/img_product_category_7-min.png" alt="Product Image . . .">
                                    <a href="toProductionDetails?proId=24"></a>
                                </div>
                                <div class="clearfix absolute name-product-no-ranking">
                                    <p class="title-product clearfix full-width title-hover-black"><a href="#">Impulse Duffle2</a></p>
                                    <p class="clearfix price-product"><span class="price-old">$802</span> $523</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Product Box -->

    <!-- Banner Full With -->
    <div class=" relative full-width bottom-margin-default homepage-bottom-margin-default">
        <div class="clearfix container-web">
            <div class=" container banner_full_width">
                <div class="row relative banners-effect5 overfollow-hidden">
                    <img src="img/banner_full_w_2.png" alt="Banner Full Width . . .">
                    <a href="#"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner Full With -->
    <!-- Support -->
    <div class=" support-box full-width clear-padding bottom-margin-default homepage-bottom-margin-default">
        <div class="container-web clearfix">
            <div class=" container border top-padding-default homepage-top-padding-default bottom-padding-default">
                <div class="row">
                    <div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
                        <img src="img/icon_free_ship.png" alt="Icon Free Ship" class="absolute" />
                        <p>free shipping</p>
                        <p>on order over $500</p>
                    </div>
                    <div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
                        <img src="img/icon_support.png" alt="Icon Supports" class="absolute">
                        <p>support</p>
                        <p>life time support 24/7</p>
                    </div>
                    <div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
                        <img src="img/icon_patner.png" alt="Icon partner" class="absolute">
                        <p>help partner</p>
                        <p>help all aspects</p>
                    </div>
                    <div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
                        <img src="img/icon_phone_big.png" alt="Icon Phone Tablet" class="absolute">
                        <p>contact with us</p>
                        <p>+07 (0) 7782 9137</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Support Box -->
</div>
<!-- End Content Box -->
<!-- Footer Box -->
<footer class="relative full-width">
    <div class=" top-footer full-width">
        <div class="clearfix container-web relative">
            <div class=" container">
                <div class="row">
                    <div class="clearfix col-md-9 col-sm-12 clear-padding col-xs-12">
                        <div class="clearfix text-subscribe text-subscribe">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <p>sign up for newsletter</p>
                            <p>Get updates on discount & counpons.</p>
                        </div>
                        <div class="clearfix form-subscribe">
                            <input type="text" name="email-subscribe" placeholder="Enter your email . . .">
                            <button class="animate-default button-hover-red">subscribe</button>
                        </div>
                    </div>
                    <div class="clearfix col-md-3 col-sm-12 col-xs-12 clear-padding social-box text-right">
                        <a href="#"><img src="img/social_tw-min.png" alt="Icon Social Twiter"></a>
                        <a href="#"><img src="img/social_fa-min.png" alt="Icon Social Facebook"></a>
                        <a href="#"><img src="img/social_int-min.png" alt="Icon Social Instagram"></a>
                        <a href="#"><img src="img/social_yt-min.png" alt="Icon Social Youtube" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix container-web relative">
        <div class=" container clear-padding">
            <div class="row">
                <div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
                    <p>my account</p>
                    <ul class="list-footer">
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Login</a></li>
                        <li><a href="#">My Cart</a></li>
                        <li><a href="#">My Wishlist</a></li>
                        <li><a href="#">My Compare</a></li>
                    </ul>
                </div>
                <div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
                    <p>information</p>
                    <ul class="list-footer">
                        <li><a href="#">Information</a></li>
                        <li><a href="#">Orders History</a></li>
                        <li><a href="#">My Wishlist</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>
                <div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
                    <p>our services</p>
                    <ul class="list-footer">
                        <li><a href="#">Product Recall</a></li>
                        <li><a href="#">Gift Vouchers</a></li>
                        <li><a href="#">Returns And Exchanges</a></li>
                        <li><a href="#">Shipping Options</a></li>
                        <li><a href="#">Terms Of Use</a></li>
                    </ul>
                </div>
                <div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
                    <p>contact us</p>
                    <ul class="icon-footer">
                        <li><i class="fa fa-home" aria-hidden="true"></i> 262 Milacina Mrest, Behansed, Paris</li>
                        <li><i class="fa fa-envelope" aria-hidden="true"></i> contact@yourcompany.com</li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i> 070-7782-9137</li>
                        <li><i class="fa fa-fax" aria-hidden="true"></i> 070-7782-9138</li>
                        <li><i class="fa fa-clock-o" aria-hidden="true"></i> 09:00 AM - 18:00 PM</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class=" bottom-footer full-width">
        <div class="clearfix container-web">
            <div class=" container">
                <div class="row">
                    <div class="clearfix col-md-7 clear-padding copyright">
                        <p>Copyright © 2020 by EngoCreative. All Rights .</p>
                    </div>
                    <div class="clearfix footer-icon-bottom col-md-5 float-right clear-padding">
                        <div class="icon_logo_footer float-right">
                            <img src="img/image_payment_footer-min.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- End Footer Box -->
<script src="js/jquery-3.3.1.min.js" defer=""></script>
<script src="js/bootstrap.min.js" defer=""></script>
<script src="js/owl.carousel.min.js" defer=""></script>
<script src="js/sync_owl_carousel.js" defer=""></script>
<script src="js/scripts.js" defer=""></script>
<style>
    .copyrights{text-indent:-9999px;height:0;line-height:0;font-size:0;overflow:hidden;}
</style>
<div class="copyrights" id="links20210126">
    Collect from <a href="http://www.cssmoban.com/"  title="网站模板">模板之家</a>
    <a href="http://cooco.net.cn/" title="组卷网">组卷网</a>
</div>
</body>

</html>