import com.shop.bean.User;
import com.shop.mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test {
    //添加删除修改需要提交事务
    public static void main(String[] args) throws IOException {
//        --读取全局配置文件（InputStream）
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
//        --生成工厂对象(SqlSessionFactory)
        SqlSessionFactory sq = new SqlSessionFactoryBuilder().build(is);
//        --获取session会话对象【链接】
        SqlSession session = sq.openSession();
//        --通过session对象对数据库进行操作
//        session.insert("com.shop.mapper.UserMapper.insertUser");//第一个是sql的调用路径

        //获取mapper对象
        UserMapper mapper = session.getMapper(UserMapper.class);
//---------------------insertUser------------------------------
//        User user = new User(0, "gec", "gec");
//        //
//        mapper.insertUser(user);
//------------------insertUser---------------------------------

//---------------------queryUser------------------------------

        List<User> list = mapper.queryUser();
        for(User user: list){
            System.out.println(user);
        }
//------------------queryUser---------------------------------


        //        --提交事务
        session.commit();
    }
}
