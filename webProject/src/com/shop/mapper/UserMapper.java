package com.shop.mapper;

import com.shop.bean.User;

import java.util.List;

//接口是用于绑定UserMapper.xml中sql操作
public interface UserMapper {

    // com.shop.mapper.UserMapper.insertUser
    //返回值类型根据需求定义，但是方法名必须要和id一致
    public int insertUser(User user);

    //查询方法
    public List<User> queryUser();

}
